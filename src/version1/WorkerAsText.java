package version1;

import version1.abstraction.IConsole;
import version1.abstraction.Worker;

public class WorkerAsText extends Worker   {

    @Override
    public void toConsole() {

        int calculated = calculate();

        System.out.println("Calculated value: " + calculated);
    }
}
