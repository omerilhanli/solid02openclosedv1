package version1;

import version1.abstraction.IConsole;
import version1.abstraction.Worker;

public class WorkerAsXml extends Worker {

    @Override
    public void toConsole() {

        int calculated = calculate();

        System.out.println("<txt>Calculated value: " + calculated + "</txt>");
    }
}
