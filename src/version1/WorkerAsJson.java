package version1;

import version1.abstraction.IConsole;
import version1.abstraction.Worker;

public class WorkerAsJson extends Worker  {

    @Override
    public void toConsole() {

        int calculated = calculate();

        System.out.println("{\"value\" : \"Calculated value: " + calculated + "\"}");
    }
}
