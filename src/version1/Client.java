package version1;

import version1.abstraction.Worker;

public class Client {

    public static void main(String[] args) {

        Worker worker = new WorkerAsText();

        worker.toConsole();

        worker = new WorkerAsHtml();

        worker.toConsole();


        worker = new WorkerAsXml();

        worker.toConsole();


        worker = new WorkerAsJson();

        worker.toConsole();

        // Yeni bir ekleme geldiğinde sadece ilgili Sub Type implementasyonu eklenerek
        // işlev başarılı bir şekilde çalıştırılabilir
        // Kodda değişiklik kapatılmış, Genişletilebilirlik sağlanmıştır.
    }
}
