package version1;

import version1.abstraction.IConsole;
import version1.abstraction.Worker;

public class WorkerAsHtml extends Worker  {

    @Override
    public void toConsole() {

        int calculated = calculate();

        System.out.println("<html><title>Calculated Value</title><body>Calculated value: " + calculated + "</body></html>");
    }
}
