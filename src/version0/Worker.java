package version0;

public class Worker {

    public void toConsole(AsEnum as) {

        int calculated = calculate();

        switch (as) {
            case TEXT:
                System.out.println("Calculated value: " + calculated);
                break;

            case HTML:
                System.out.println("<html><title>Calculated Value</title><body>Calculated value: " + calculated + "</body></html>");
                break;

            case XML:
                System.out.println("<txt>Calculated value: " + calculated + "</txt>");
                break;

            case JSON:
                System.out.println("{\"value\" : \"Calculated value: " + calculated + "\"}");
                break;
        }

    }

    private int calculate() {

        return 1 * 2 * 3;
    }

    public enum AsEnum {

        TEXT, HTML, XML, JSON
    }
}
